package com.test.rindus.controller;

import java.util.List;
import java.util.Optional;

import com.test.rindus.model.BankData;

public interface IBankDataController {
	
	List<BankData> getAllData();
	
	Optional<BankData> getDataById(Long id);
	
	BankData addData(BankData data);
	
	String deleteData(Long id);
	
	String updateData(BankData updatedData);

}
