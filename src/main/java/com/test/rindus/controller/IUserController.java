package com.test.rindus.controller;

import java.util.List;
import java.util.Optional;

import com.test.rindus.model.User;

public interface IUserController {
	
	List<User> getAllUsers();
	
	Optional<User> getUserById(Long id);
	
	User addUser(User user);
	
	String deleteUser(Long id);
	
	String updateUser(User updatedUser);

}
