package com.test.rindus.security.service;

import java.util.List;
import java.util.Optional;

import com.test.rindus.model.BankData;

public interface BankDataService {
	
	List<BankData> findAllData();
	
	Optional<BankData> findDataById(Long id);
	
	BankData saveData(BankData newBankData);
	
	String deleteData(Long id);
	
	String updateData(BankData updatedData);

}
