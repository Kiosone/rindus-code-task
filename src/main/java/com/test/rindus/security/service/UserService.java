package com.test.rindus.security.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.test.rindus.model.BankData;
import com.test.rindus.model.User;

@Service
public interface UserService {

//	Optional<Long> login(String username, String password);
//	
	Optional<User> findByToken(String token);
//	
//	void logout(User user);
	
	List<User> findAllUsers();
	
	Optional<User> findUserById(Long id);
	
	User saveUser(User newUser);
	
	String deleteUser(Long id);
	
	String updateUser(User updatedUser);
	
}
