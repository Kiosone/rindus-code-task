package com.test.rindus.security.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.validator.routines.IBANValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.rindus.model.BankData;
import com.test.rindus.repository.BankDataRepository;
import com.test.rindus.security.service.BankDataService;

@Service
public class BankDataServiceImpl implements BankDataService {
	
	@Autowired
	BankDataRepository dataRepo;

	@Override
	public List<BankData> findAllData() {	
		return dataRepo.findAll();
	}

	@Override
	public Optional<BankData> findDataById(Long id) {
		return dataRepo.findById(id);
	}

	@Override
	public BankData saveData(BankData newBankData) {
		if(newBankData != null) {
			if(IBANValidator.getInstance().isValid(newBankData.getIban())) {
				return dataRepo.save(newBankData);
			}
		}
		return new BankData();
	}

	@Override
	public String deleteData(Long id) {
		if(dataRepo.findById(id).isPresent()) {
			dataRepo.deleteById(id);
			return "Data deleted correctly!";
		}
		return "This id doesn't exist!";
	}

	@Override
	public String updateData(BankData updatedData) {
		if(dataRepo.findById(updatedData.getId()).isPresent()) {
			BankData newData = new BankData();
			newData.setId(updatedData.getId());
			newData.setName(updatedData.getName());
			newData.setLastname(updatedData.getLastname());
			newData.setIban(updatedData.getIban());
			dataRepo.save(newData);
			return "Data modified correctly!";
		}
		return "Cannot modify data!";
	}

}
