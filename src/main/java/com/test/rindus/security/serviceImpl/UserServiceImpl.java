package com.test.rindus.security.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.rindus.model.BankData;
import com.test.rindus.model.User;
import com.test.rindus.repository.UserRepository;
import com.test.rindus.security.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepo;

	/*@Override
	public Optional<Long> login(final String username, final String password) {
		final User user = new User();
		user.setUser(username);
		user.setPassword(password);
		
		return Optional.of(user.getId());
	}
	
	@Override
	public Optional<User> findByToken(final String token) {
		return userRepo.findById(token);
	}
	
	@Override
	public void logout(final User user) {
		
	}*/

	@Override
	public Optional<User> findByToken(String token) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findAllUsers() {
		return userRepo.findAll();
	}

	@Override
	public Optional<User> findUserById(Long id) {
		return userRepo.findById(id);
	}

	@Override
	public User saveUser(User newUser) {
		if(newUser != null) {
			return userRepo.save(newUser);
		}
		return new User();
	}

	@Override
	public String deleteUser(Long id) {
		if(userRepo.findById(id).isPresent()) {
			userRepo.deleteById(id);
			return "Data deleted correctly!";
		}
		return "This id doesn't exist!";
	}

	@Override
	public String updateUser(User updatedUser) {
		if(userRepo.findById(updatedUser.getId()).isPresent()) {
			User newUser = new User();
			newUser.setId(updatedUser.getId());
			newUser.setUser(updatedUser.getUser());
			newUser.setPassword(updatedUser.getPassword());
			userRepo.save(newUser);
			return "Data modified correctly!";
		}
		return "Cannot modify data!";
	}

}
