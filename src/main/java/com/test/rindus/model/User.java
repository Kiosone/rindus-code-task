package com.test.rindus.model;

/*import java.util.ArrayList;
import java.util.Collection;*/

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;*/

import lombok.Data;
// import lombok.Setter;

@Entity
@Data
@Table(name="users")
public class User {

	@Id
	@Column(name="idusers")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	
	@Column(name="user")
	String user;
	
	@Column(name="password")
	String password;
	
	/*@JsonCreator
	User(@JsonProperty("id") final String id,
			@JsonProperty("user") final String user,
			@JsonProperty("password") final String password) {
		super();
		this.id = requireNonNull(id);
		this.user = requireNonNull(user);
		this.password = requireNonNull(password);
	}*/

	/*@JsonIgnore
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return new ArrayList<>();
	}
	
	@JsonIgnore
	@Override
	public String getPassword() {
		return password;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@JsonIgnore
	@Override
	public String getUsername() {
		return user;
	}*/
}
