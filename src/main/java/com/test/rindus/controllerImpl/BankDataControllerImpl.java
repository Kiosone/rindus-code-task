package com.test.rindus.controllerImpl;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.rindus.controller.IBankDataController;
import com.test.rindus.model.BankData;
import com.test.rindus.security.serviceImpl.BankDataServiceImpl;

import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@RestController
@RequestMapping("/data")
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
public class BankDataControllerImpl implements IBankDataController {
	
	@Autowired
	BankDataServiceImpl service;

	@GetMapping(value="/all", produces="application/json")
	@Override
	public List<BankData> getAllData() {
		return service.findAllData();
	}

	@GetMapping(value="/{id}", produces="application/json")
	@Override
	public Optional<BankData> getDataById(@PathVariable Long id) {
		return service.findDataById(id);
	}

	@PostMapping(value="/add", produces="application/json")
	@Override
	public BankData addData(@RequestBody BankData data) {
		return service.saveData(data);
	}

	@DeleteMapping(value="/delete/{id}", produces="application/json")
	@Override
	public String deleteData(@PathVariable Long id) {
		return service.deleteData(id);
	}

	@PutMapping(value="/update", produces="application/json")
	@Override
	public String updateData(@RequestBody BankData updatedData) {
		return service.updateData(updatedData);
	}

}
