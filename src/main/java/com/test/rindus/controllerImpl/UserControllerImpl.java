package com.test.rindus.controllerImpl;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.rindus.controller.IUserController;
import com.test.rindus.model.User;
import com.test.rindus.repository.UserRepository;
import com.test.rindus.security.serviceImpl.UserServiceImpl;

import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@RestController
@RequestMapping("/users")
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
final class UserControllerImpl implements IUserController {
	
	// @Autowired
	// UserRepository userRepo;
	
	@Autowired
	UserServiceImpl service;
	
	/*@PostMapping("/register")
	Long register(
	    @RequestParam("user") final String username,
	    @RequestParam("password") final String password) {
		User user = new User();
		user.setUser(username);
		user.setPassword(password);
		userRepo.save(user);

	    return login(username, password);
	  }

	  @PostMapping("/login")
	  Long login(
	    @RequestParam("user") final String user,
	    @RequestParam("password") final String password) {
	    return service
	      .login(user, password)
	      .orElseThrow(() -> new RuntimeException("invalid login and/or password"));
	  }*/

	@Override
	@GetMapping(value="/all", produces="application/json")
	public List<User> getAllUsers() {
		return service.findAllUsers();
	}

	@Override
	@GetMapping(value="/{id}", produces="application/json")
	public Optional<User> getUserById(@PathVariable Long id) {
		return service.findUserById(id);
	}

	@Override
	@PostMapping(value="/register", produces="application/json")
	public User addUser(@RequestBody User user) {
		return service.saveUser(user);
	}

	@Override
	@DeleteMapping(value="/delete/{id}", produces="application/json")
	public String deleteUser(@PathVariable Long id) {
		return service.deleteUser(id);
	}

	@Override
	@PutMapping(value="/update", produces="application/json")
	public String updateUser(@RequestBody User updatedUser) {
		return service.updateUser(updatedUser);
	}

}
