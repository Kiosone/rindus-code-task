package com.test.rindus.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.rindus.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	User save(User user);
	
	Optional<User> findById(String id);	
	
	Optional<User> findByUser(String user);

}
