package com.test.rindus.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.rindus.model.BankData;

@Repository
public interface BankDataRepository extends JpaRepository<BankData, Long> {
	
	void save(Optional<BankData> bankDataToUpdate);

}
